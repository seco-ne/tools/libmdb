SUBDIRS := test lib

all: $(SUBDIRS)
install: $(SUBDIRS)
	$(MAKE) -C lib install
	$(MAKE) -C test install

clean:
	$(MAKE) -C lib clean
	$(MAKE) -C test clean

lib:
	$(MAKE) -C $@
test: lib
	$(MAKE) -C $@

.PHONY: all $(SUBDIRS)
