/************************************************************************/
/* Copyright (C) 2012 Garz&Fricke GmbH					 				*/
/*		No use or disclosure of this information in any form without	*/
/*		the written permission of the author							*/
/************************************************************************/

/************************************************************************/
/*                                                                      */
/*  DESCRIPTION                                                         */
/*                                                                      */
/*  This is a test tool for MDB. It attempts to connect to a cashless   */
/*  device on ttymxc1 and sends configuration data to it. On success,   */
/*  the response is printed on the standard output. ttymxc1 has to      */
/*  support MARK and SPACE parity, otherwise the communication will     */
/*  fail. This test has been tried with Garz & Fricke KarL� on VINCELL  */
/*  with MDB master option.                                             */
/*																		*/
/************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <libmdb.h>

#define MDB_ADDRESS		0x10
#define RETRIES			10

static char *default_dev = "ttymxc1";

static void print_help()
{
    printf("mdbtest_master\tTests the MDB master communication\n");
    printf("usage: mdbtest_master [-h] [devicename]\n");
    printf("\t-h|--help \tPrints this help\n");
    printf("\tdevicename\tName of the MDB tty device (default: ttymxc1)\n");
}

void evaluate_reply(unsigned char *data, int len)
{
	if (len == 0)
	{
		printf("Received ACK from client\n");
	}
	else
	{
		int i;
		printf("Received %d bytes from client: ", len);
		for (i = 0; i < len; i++)
		{
			printf("0x%02x ", data[i]);
		}
		printf("\n");
	}
}

int main(int argc, char **argv)
{
	unsigned char reset_data[] = { 0x00 };
	unsigned char poll_data[] = { 0x02 };
	unsigned char config_data[] = { 0x01, 0x00, 0x83, 0x10, 0x02, 0x01 };
	unsigned char get_id_data[] = { 0x07, 0x00, 0x47, 0x55, 0x46, 0x30,
									0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
									0x30, 0x30, 0x30, 0x30, 0x30, 0x32,
									0x30, 0x30, 0x32, 0x33, 0x32, 0x31,
									0x20, 0x00, 0x20, 0x4B, 0x6F, 0x03 };
	unsigned char diag_data[] = { 0x07, 0xff, 0x05, 0x06, 0x12,
								  0x44, 0x52, 0x41, 0x56, 0x50 };
	unsigned char recv_data[MAX_MDB_MESSAGE_LENGTH];
	int mdb_handle, recv_len, ret, i;
	struct mdb_settings settings = {
		.size = sizeof(struct mdb_settings),
		.reset_time_ms = DEFAULT_RESET_TIME_MS,
		.recv_timeout_ms = 15,
	};
    
    char *device;

    while (1) {
        int c;
        //int this_option_optind = optind ? optind : 1;
        int option_index = 0;
        static struct option long_options[] = {
           {"help", no_argument, 0, 0 },
           {0,                0, 0, 0 }
        };
        c = getopt_long(argc, argv, "h", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
        case 0:
            if (option_index == 0) {
                print_help();
                return 0;
            }
            break;
        case 'h':
            print_help();
            return 0;
        }
    }
    
    device =  (optind < argc) ? argv[optind] : default_dev;
    
	printf("Opening %s for MDB\n", device);
	ret = mdb_open(&mdb_handle, device);
	if (ret)
	{
		printf("Could not open %s for MDB (error %d)\n", device, ret);
		return 1;
	}
    
	printf("Setting MDB configuration\n");
	ret = mdb_set_settings(mdb_handle, &settings);
	if (ret)
	{
		printf("Could not set MDB configuration\n");
		goto error;
	}

	printf("Asserting wakeup line\n");
	ret = mdb_wakeup(mdb_handle, 1);
	if (ret)
	{
		printf("Could not assert wakeup line (error %d)\n", ret);
		goto error;
	}

	printf("Resetting the MDB bus\n");
	ret = mdb_reset(mdb_handle);
	if (ret)
	{
		printf("Could not reset the MDB bus (error %d)\n", ret);
		goto error;
	}

	printf("Sending reset command to client\n");
	ret = -1;
	for (i = 0; i < RETRIES; i++)
	{
		recv_len = MAX_MDB_MESSAGE_LENGTH;
		ret = mdb_transaction(mdb_handle, MDB_ADDRESS, reset_data,
								sizeof(reset_data), recv_data, &recv_len);
		if (ret == 0)
		{
			evaluate_reply(recv_data, recv_len);
			break;
		}
		usleep(100*1000);
	}
	if (i == RETRIES)
	{
		printf("MDB transceive error (error %d).\n", ret);
		goto error;
	}

	printf("Sending poll command to client\n");
	ret = -1;
	for (i = 0; i < RETRIES; i++)
	{
		recv_len = MAX_MDB_MESSAGE_LENGTH;
		ret = mdb_transaction(mdb_handle, MDB_ADDRESS, poll_data,
								sizeof(poll_data), recv_data, &recv_len);
		if (ret == 0)
		{
			evaluate_reply(recv_data, recv_len);
			break;
		}
		usleep(100*1000);
	}
	if (i == RETRIES)
	{
		printf("MDB transceive error (error %d).\n", ret);
		goto error;
	}

	printf("Sending config command to client\n");
	ret = -1;
	for (i = 0; i < RETRIES; i++)
	{
		recv_len = MAX_MDB_MESSAGE_LENGTH;
		ret = mdb_transaction(mdb_handle, MDB_ADDRESS, config_data,
								sizeof(config_data), recv_data, &recv_len);
		if (ret == 0)
		{
			evaluate_reply(recv_data, recv_len);
			break;
		}
		usleep(100*1000);
	}
	if (i == RETRIES)
	{
		printf("MDB transceive error (error %d).\n", ret);
		goto error;
	}

	printf("Sending get ID command to client\n");
	ret = -1;
	for (i = 0; i < RETRIES; i++)
	{
		recv_len = MAX_MDB_MESSAGE_LENGTH;
		ret = mdb_transaction(mdb_handle, MDB_ADDRESS, get_id_data,
								sizeof(get_id_data), recv_data, &recv_len);
		if (ret == 0)
		{
			evaluate_reply(recv_data, recv_len);
			break;
		}
		usleep(100*1000);
	}
	if (i == RETRIES)
	{
		printf("MDB transceive error (error %d).\n", ret);
		goto error;
	}

	printf("Sending diag command to client\n");
	ret = -1;
	for (i = 0; i < RETRIES; i++)
	{
		recv_len = MAX_MDB_MESSAGE_LENGTH;
		ret = mdb_transaction(mdb_handle, MDB_ADDRESS, diag_data,
								sizeof(diag_data), recv_data, &recv_len);
		if (ret == 0)
		{
			evaluate_reply(recv_data, recv_len);
			break;
		}
		usleep(100*1000);
	}
	if (i == RETRIES)
	{
		printf("MDB transceive error (error %d).\n", ret);
		goto error;
	}

	mdb_wakeup(mdb_handle, 0);
	mdb_close(mdb_handle);
	return 0;

error:
	mdb_wakeup(mdb_handle, 0);
	mdb_close(mdb_handle);
	return 1;
}
