/************************************************************************/
/* Copyright (C) 2014 Garz&Fricke GmbH					 				*/
/*		No use or disclosure of this information in any form without	*/
/*		the written permission of the author							*/
/************************************************************************/

/************************************************************************/
/*                                                                      */
/*  DESCRIPTION                                                         */
/*                                                                      */
/*  This is a test tool for MDB slave. It waits for initialization      */
/*  commands from a VMC and responds to it. ttymxc1 has to support MARK */
/*  and SPACE parity, otherwise the communication will fail. This test  */
/*  has been tried with Garz & Fricke VMC AUSTER v1.0 on VINCELL LT     */
/*  with MDB slave option.                                              */
/*																		*/
/************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <libmdb.h>

#define RESET_BYTE				0x10			// cashless device
#define POLL_BYTE				0x12
#define SETUP_BYTE				0x11
#define SETUP_CONFIG_DATA_BYTE	0x00
#define SETUP_PRICES_BYTE		0x01
#define EXPANSION_BYTE			0x17
#define EXPANSION_REQ_ID_BYTE	0x00
#define READER_BYTE				0x14
#define READER_ENABLE_BYTE		0x01

enum state {
	UNINITIALIZED = 0,
	RESET,
	CONFIG_DATA_SETUP,
	PRICES_SETUP,
	ID_REQUESTED,
	READER_ENABLED,
};

static char *default_dev = "ttymxc1";

static void print_help()
{
    printf("mdbtest_slave\tTests the MDB slave communication\n");
    printf("usage: mdbtest_slave [-h] [devicename]\n");
    printf("\t-h|--help \tPrints this help\n");
    printf("\tdevicename\tName of the MDB tty device (default: ttymxc1)\n");
}


int main(int argc, char **argv)
{
	int mdb_handle, ret = 0, recv_len, send_len, i;
	struct timespec wait_timeout;
	unsigned int read_timeout_us;
	unsigned int poll_count;
	unsigned char recv_data[MAX_MDB_MESSAGE_LENGTH+1];
	unsigned char reset_data[] = { 0x00 };
	unsigned char setup_config_data[] = { 0x01, 0x01, 0x18, 0x40, 0x01, 0x02, 0x0a, 0x00 };
	unsigned char expansion_req_id_data[] = { 0x09, 0x47, 0x55, 0x46, 0x31, 0x32, 0x33, 0x34,
											0x35, 0x36, 0x37, 0x38, 0x39, 0x31, 0x31, 0x31,
											0x4b, 0x61, 0x72, 0x4c, 0x20, 0x30, 0x32, 0x2e,
											0x32, 0x31, 0x20, 0x20, 0x02, 0x01, 0xc0, 0x00,
											0x00, 0x06 };
	unsigned char *precv_data;
	unsigned char *psend_data;
	enum state state = RESET;
    char *device;

    while (1) {
        int c;
        //int this_option_optind = optind ? optind : 1;
        int option_index = 0;
        static struct option long_options[] = {
           {"help", no_argument, 0, 0 },
           {0,                0, 0, 0 }
        };
        c = getopt_long(argc, argv, "h", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
        case 0:
            if (option_index == 0) {
                print_help();
                return 0;
            }
            break;
        case 'h':
            print_help();
            return 0;
        }
    }
    
    device =  (optind < argc) ? argv[optind] : default_dev;

	printf("Opening %s for MDB\n", device);
	ret = mdb_slave_open(&mdb_handle, device, POLL_BYTE);
	if (ret)
	{
		printf("Could not open %s for MDB (error %d)\n", device, ret);
		return 1;
	}

	wait_timeout.tv_sec = 5;
	wait_timeout.tv_nsec = 0;

	/*
	 * Wait for messages in a loop
	 */
	printf("Waiting for data...\n");
	while (true)
	{
		ret = mdb_slave_wait_for_message(mdb_handle, &wait_timeout);
		if (ret != 0)
			break;
		precv_data = recv_data;
		read_timeout_us = 1000000;
		psend_data = MDB_ACK;
		send_len = 0;

		/*
		 * Message is available, start reading data. We do this in a
		 * loop as well, because the message may not have completely
		 * arrived yet when calling read for the first time.
		 */
		while (ret == 0 && read_timeout_us > 0)
		{
			recv_len = sizeof(recv_data)/sizeof(recv_data[0]) - (precv_data - recv_data);
			ret = mdb_slave_read_message(mdb_handle, precv_data, &recv_len);
			if (ret == 0 && recv_len > 0) {
				precv_data += recv_len;
			}
			recv_len = precv_data - recv_data;

			/*
			 * After reading, check if we have received a complete command
			 * by evaluating the message length and the first two data bytes.
			 * If message is complete, break out of the read loop.
			 */
			if (recv_len == 2)
			{
				/*
				 * The reset command has to be handled separately, because its
				 * response may not be sent immediately but rather with the
				 * next POLL command.
				 */
				if (recv_data[0] == RESET_BYTE)
				{
					ret = mdb_slave_verify_message_and_reply(mdb_handle, recv_data, &recv_len, MDB_ACK, 0);
					printf("Got message (1): 0x%02x\n", RESET_BYTE);
					if (ret == 0) {
						mdb_slave_send_reply(mdb_handle, reset_data, sizeof(reset_data));
						printf("   --> My reply:");
						for (i=0; i<sizeof(reset_data); i++)
							printf(" 0x%02x", reset_data[i]);
						printf("\n");
						state = RESET;
					} else {
						printf("   --> CHK byte did not match!\n");
					}
					break;
				}
			}
			else if (recv_len == 3)
			{
				if (recv_data[0] == READER_BYTE && recv_data[1] == READER_ENABLE_BYTE)
				{
					if (state == ID_REQUESTED)
						state = READER_ENABLED;
					break;
				}
			}
			else if (recv_len == 7)
			{
				if (recv_data[0] == SETUP_BYTE && recv_data[1] == SETUP_CONFIG_DATA_BYTE)
				{
					psend_data = setup_config_data;
					send_len = sizeof(setup_config_data);
					if (state == RESET)
						state = CONFIG_DATA_SETUP;
					break;
				}
				if (recv_data[0] == SETUP_BYTE && recv_data[1] == SETUP_PRICES_BYTE)
				{
					if (state == CONFIG_DATA_SETUP)
						state = PRICES_SETUP;
					break;
				}
			}
			else if (recv_len == 31)
			{
				if (recv_data[0] == EXPANSION_BYTE && recv_data[1] == EXPANSION_REQ_ID_BYTE)
				{
					psend_data = expansion_req_id_data;
					send_len = sizeof(expansion_req_id_data);
					if (state == PRICES_SETUP)
						state = ID_REQUESTED;
					break;
				}
			}
			usleep(50);
			read_timeout_us -= 50;
		}

		/*
		 * Message is complete. Verify and send reply if valid.
		 */
		if (recv_data[0] != RESET_BYTE)
		{
			recv_len = precv_data - recv_data;
			ret = mdb_slave_verify_message_and_reply(mdb_handle, recv_data, &recv_len, psend_data, send_len);
			printf("Got message (%d):", recv_len);
			for (i=0; i<recv_len; i++)
				printf(" 0x%02x", recv_data[i]);
			printf("\n");
			if (ret < 0) {
				printf("   --> CHK byte did not match!\n");
				state--;
			} else {
				printf("   --> My reply:");
				if (psend_data == MDB_ACK)
					printf(" ACK");
				for (i=0; i<send_len; i++)
					printf(" 0x%02x", psend_data[i]);
				printf("\n");
			}
		}

		/*
		 * Check if initialization is complete.
		 */
		if (state == READER_ENABLED) {
			printf("MDB device initialized successfully!\n");
			printf("Replying to POLL commands for 5 more seconds...\n");
		}
	}

	mdb_slave_get_poll_count(mdb_handle, &poll_count);
	printf("POLL count: %d\n", poll_count);

	mdb_slave_close(mdb_handle);
	return 0;
}
