/*
 * mdb - Library for using the Multi-Drop Bus (MDB) over a MARK/SPACE
 *       parity supporting TTY serial interface.
 *       Slave implementation
 *
 * Author: Tim Jaacks  <tim.jaacks@garz-fricke.com>
 * Copyright (C) 2012-2014 Garz & Fricke GmbH
 */

#include <asm/ioctls.h>
#include <errno.h>
#include <fcntl.h>
#include <gpiod.h>
#include <linux/serial.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#include "libmdb.h"
#include "libmdb_common.h"

struct message_queue_entry
{
	TAILQ_ENTRY(message_queue_entry) entries;
	unsigned char message[MAX_MDB_MESSAGE_LENGTH+1];
	unsigned char bytes, read;
};

struct mdb_slave_handle
{
	char *tty_dev;
	int tty_fd;
	char pollbyte;
	pthread_t tid;
	bool thread_running;
	unsigned int poll_count;
	sem_t message_available;
	unsigned char *pending_reply;
	int pending_reply_length;
	TAILQ_HEAD(message_queue_head, message_queue_entry) message_queue;
	struct gpiod_line * mdb_wakeup_out;
};

inline void add_byte_to_message_buffer(struct message_queue_entry *pmq_entry, unsigned char byte)
{
	if (pmq_entry != NULL && pmq_entry->bytes < MAX_MDB_MESSAGE_LENGTH+1)
	{
		pmq_entry->message[pmq_entry->bytes] = byte;
		pmq_entry->bytes++;
	}
}

void send_message(struct mdb_slave_handle *pmdb, unsigned char *pdata, int length)
{
	struct termios tio;
	unsigned char last_byte;

	if (pdata != MDB_ACK) {
		/*
		 * If this is a regular message, send the message bytes first
		 * without the 9th bit set.
		 */
		write(pmdb->tty_fd, pdata, length);
		last_byte = calculate_chk(pdata, length);
	} else {
		last_byte = MDB_ACK;
	}

	/* Switch to mark parity for the last byte */
	tcgetattr(pmdb->tty_fd, &tio);
	tio.c_cflag |= PARODD; // CMSPAR | PARODD = MARK parity
	tcsetattr(pmdb->tty_fd, TCSANOW , &tio);

	/* Send the last byte */
	write(pmdb->tty_fd, &last_byte, 1);

	/* Switch back to space parity */
	tio.c_cflag &= ~PARODD; // CMSPAR & ~PARODD = SPACE parity
	tcsetattr(pmdb->tty_fd, TCSANOW , &tio);
}

void* receive_thread(void *arg)
{
	struct mdb_slave_handle *pmdb = arg;
	struct message_queue_entry *pmq_entry;
	unsigned char buffer[4];
	int ret, i;
	fd_set fds;
	struct timeval tv;

	/* select timeout 1 ms */
	tv.tv_sec = 0;
	tv.tv_usec = 1000;
	i = 0;

	/*
	 * Create first dummy message queue entry in case we receive any remaining
	 * bytes of an ongoing message, issued by the MDB master before we started
	 * listening.
	 */
	pmq_entry = calloc(1, sizeof(struct message_queue_entry));
	TAILQ_INSERT_HEAD(&pmdb->message_queue, pmq_entry, entries);

	/* listen on the serial port continuously */
	while(pmdb->thread_running)
	{
		FD_ZERO(&fds);
		FD_SET(pmdb->tty_fd, &fds);

		ret = select(pmdb->tty_fd + 1, &fds, NULL, NULL, &tv);
		if(ret > 0)
		{
			/* read one byte at a time */
			ret = read(pmdb->tty_fd, &buffer[i], 1);
			if (ret > 0)
			{
				switch (i) {
				case 0: 
					if (buffer[i] == 0xff) {
						/*
						 * Escape byte 0xff received. We have to wait for the next
						 * byte in order to determine what this means.
						 */
						i++;
					} else {
						/*
						 * Just an ordinary byte. Append it to the message buffer.
						 */
						add_byte_to_message_buffer(pmq_entry, buffer[i]);
					}
					break;

				case 1:
					if (buffer[i] == 0xff) {
						/*
						 * We received two subsequent 0xff bytes, which means that one
						 * 0xff byte has actually been sent. The second byte has been added
						 * to separate between a parity error sequence and a "real" 0xff.
						 * Thus we have to skip one of the two bytes.
						 */
						add_byte_to_message_buffer(pmq_entry, buffer[i]);
						i = 0;
					} else if (buffer[i] == 0x00) {
						/*
						 * We received the byte sequence 0xff 0x00, which means that a parity
						 * error occured on the next byte.
						 */
						i++;
					} else {
						/*
						 * We received a single 0xff followed by a byte different from 0x00.
						 * This case is undefined, it may not occur. If it happens, though,
						 * add the bytes to the message buffer.
						 */
						add_byte_to_message_buffer(pmq_entry, buffer[i-1]);
						add_byte_to_message_buffer(pmq_entry, buffer[i]);
						i = 0;
					}
					break;

				case 2:
					/*
					 * A parity error occured on this byte, which means that the 9th
					 * bit was set. This is supposed to be the start of a message.
					 */
					if (0 == pmdb->pollbyte){
						/* 
						 * All message mode is enabled, will queue every message.
						 */
						pmq_entry = calloc(1, sizeof(struct message_queue_entry));
						TAILQ_INSERT_HEAD(&pmdb->message_queue, pmq_entry, entries);
						add_byte_to_message_buffer(pmq_entry, buffer[i]);
						sem_post(&pmdb->message_available);
						i = 0;
					} else {
						if (buffer[i] == pmdb->pollbyte) {
						/*
						 * We have received the start of a POLL message. Step forward to the
						 * next state in which we expect to receive the according CHK.
						 */
						i++;
						} else if ((buffer[i] & MDB_ADDRESS_MASK) == (pmdb->pollbyte & MDB_ADDRESS_MASK)) {
							/*
							* We have received the start of any other than the POLL command.
							* Create a new message buffer in the message queue and signify
							* that there is new data available.
							*/
							pmq_entry = calloc(1, sizeof(struct message_queue_entry));
							TAILQ_INSERT_HEAD(&pmdb->message_queue, pmq_entry, entries);
							add_byte_to_message_buffer(pmq_entry, buffer[i]);
							sem_post(&pmdb->message_available);
							i = 0;
						} else {
							/*
							* We have received the start of a command which is not addressed to
							* this client. Ignore the complete message.
							*/
							pmq_entry = NULL;
							i = 0;
						}
					}
					break;

				case 3:
					if (buffer[i] == buffer[i-1]) {
						/*
						 * We have received a complete POLL command. Reply with an ACK or,
						 * if existing, with a pending message.
						 */
						if (pmdb->pending_reply) {
							send_message(pmdb, pmdb->pending_reply, pmdb->pending_reply_length);
							free(pmdb->pending_reply);
							pmdb->pending_reply = NULL;
						} else {
							send_message(pmdb, MDB_ACK, 0);
						}
						pmdb->poll_count++;
					} else {
						/*
						 * CHK did not match. Do nothing.
						 */
					}
					/*
					 * Reset the state machine. The next received byte should be the start
					 * of a new command with the 9th bit set, which would result in a new
					 * message buffer being created. However, if other bytes follow before
					 * a new command is recognized (which may be an ACK sent by the master),
					 * ignore them by setting the current message buffer to NULL.
					 */
					pmq_entry = NULL;
					i = 0;
					break;
				}
			}
		}
	}

	return NULL;
}

/*****************************************************************
 *Function name	: mdb_slave_open
*/
/**
 * @brief	Opens the given serial port and configures it for MDB
 *			slave mode. If a pollbyte is given (!= 0),
 *			All POLL messages received on this port are
 *			automatically acknowledged with an ACK as soon as this
 *			function returns until mdb_slave_close is called.
 *			This function returns a handle in the first parameter,
 *			which is needed for all further library calls.
 *			The process priority is increased by calling nice(-15)
 *			implicitly. This is necessary for appropriate bus timings.
 *			If you encounter delays between single bytes of a message
 *			please check priorities of other processes and adjust
 *			your program's priority accordingly. The need for
 *			increasing the priority requires the calling program to
 *			run with root privileges.
 *
 * @param	handle		Pointer to variable to store handle into
 * @param	tty_dev		String containing the TTY device to use (e.g. "ttymxc1")
 * @param	pollbyte	Poll byte to respond to, includes device address in the upper five bits.
 * 						If 0, will respond to every message.
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid parameter
 * @retval	-EIO		TTY port could not be opened or configured
 * @retval	-EPERM		Process priority could not be set
*/
int mdb_slave_open(int *handle, char* tty_dev, char pollbyte)
{
	int ret = 0;
	struct mdb_slave_handle *pmdb;

	if (!handle || !tty_dev)
		return -EINVAL;

	pmdb = (struct mdb_slave_handle *) malloc(sizeof(struct mdb_slave_handle));
	if (!pmdb)
		return -EIO;

	/* increase process priority in order to get appropriate timings */
	errno = 0;
	ret = nice(-15);
	if (ret == -1 && errno != 0)
		return -EPERM;

	/* open serial port */
	ret = init_serial_port(tty_dev, &pmdb->tty_fd);
	if (ret < 0)
		goto open_failed;

	pmdb->tty_dev = tty_dev;
	pmdb->pollbyte = pollbyte;
	pmdb->poll_count = 0;

	/* create semaphore */
	ret = sem_init(&pmdb->message_available, 0, 0);
	if (ret < 0)
		goto semaphore_failed;

	/* create poll respond thread */
	pmdb->thread_running = true;
	ret = pthread_create(&pmdb->tid, NULL, receive_thread, pmdb);
	if (ret < 0)
		goto thread_creation_failed;

	/* initialize message queue */
	TAILQ_INIT(&pmdb->message_queue);

	pmdb->mdb_wakeup_out = mdb_open_wakeup_out();

	*handle = (int)pmdb;
	return 0;

thread_creation_failed:
	sem_destroy(&pmdb->message_available);
semaphore_failed:
	close(pmdb->tty_fd);
open_failed:
	free(pmdb);
	return ret;
}

/*****************************************************************
 *Function name	: mdb_slave_close
*/
/**
 * @brief	Closes the serial port and frees all resources.
 *
 * @param	handle		Handle identifying caller (returned by mdb_master_open)
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle
*/
int mdb_slave_close(int handle)
{
	struct mdb_slave_handle *pmdb = (struct mdb_slave_handle*) handle;

	if (!pmdb)
		return -EINVAL;

	pmdb->thread_running = false;
	pthread_join(pmdb->tid, NULL);

	sem_destroy(&pmdb->message_available);

	close(pmdb->tty_fd);

	mdb_close_wakeup_out(pmdb->mdb_wakeup_out);

	free(pmdb);

	return 0;
}

/*****************************************************************
 *Function name	: mdb_slave_wait_for_message
*/
/**
 * @brief	Waits for a message from the master addressed to this
 *			slave. If the timeout values are 0, this function returns
 *			immediately. If timeout is NULL, it waits indefinitely.
 *
 * @param	handle		Handle identifying caller (returned by mdb_slave_open)
 * @param	timeout		Timeout
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle
 * @retval	-ETIMEDOUT	No message has been received within the given timeout
*/
int mdb_slave_wait_for_message(int handle, struct timespec *timeout)
{
	struct mdb_slave_handle *pmdb = (struct mdb_slave_handle*) handle;
	struct message_queue_entry *pmq_entry;
	struct timespec ts;
	int ret;

	if (!pmdb)
		return -EINVAL;

	if (timeout)
	{
		clock_gettime(CLOCK_REALTIME, &ts);
		ts.tv_sec += timeout->tv_sec;
		ts.tv_nsec += timeout->tv_nsec;
		if (ts.tv_nsec >= 1000000000L) {
			ts.tv_nsec -= 1000000000L;
			ts.tv_sec++;
		}
		ret = sem_timedwait(&pmdb->message_available, &ts);
	}
	else
		ret = sem_wait(&pmdb->message_available);

	if (ret == 0)
	{
		/*
		 * If wait has returned successfully, a new message is available in
		 * the message queue. Remove the old message.
		 */
		pmq_entry = TAILQ_LAST(&pmdb->message_queue, message_queue_head);
		TAILQ_REMOVE(&pmdb->message_queue, pmq_entry, entries);
		if (pmq_entry) {
			free (pmq_entry);
		}
	}
	return ret;
}

/*****************************************************************
 *Function name	: mdb_slave_read_message
*/
/**
 * @brief	Reads bytes of the current message (if there are any)
 *			and returns immediately. Has to be called after
 *			mdb_slave_wait_for_message(). As soon as the start of a
 *			new message is recognized and all bytes of the current
 *			message have been read, this function returns ENODATA
 *			until mdb_slave_wait_for_message() is called again.
 *			Since this function cannot recognize the end of the
 *			message by itself, it does not evaluate and verify the
 *			CHK byte. Thus, maximum data length is 37.
 *
 * @param	handle		Handle identifying caller (returned by mdb_slave_open)
 * @param	precv_data	Pointer to the memory where the received data shall be stored
 * @param	precv_len	Pointer to the length of the receive array. After returning,
 *						precv_len contains the length of the received data (0..37).
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle or precv_data is NULL or *precv_len is not within 1..37
 * @retval	-ENXIO		No message available, call mdb_slave_wait_for_message() before
 * @retval	-ENODATA	No more bytes left in this message
 * @retval	-EIO		TTY error
*/
int mdb_slave_read_message(int handle, unsigned char *precv_data, int *precv_len)
{
	struct mdb_slave_handle *pmdb = (struct mdb_slave_handle*) handle;
	struct message_queue_entry *pmq_entry;
	int count;

	if (!pmdb || !precv_data || !precv_len || *precv_len < 1 ||
			*precv_len > MAX_MDB_MESSAGE_LENGTH+1)
		return -EINVAL;

	pmq_entry = TAILQ_LAST(&pmdb->message_queue, message_queue_head);

	if (!pmq_entry)
		return -ENXIO;

	count = pmq_entry->bytes - pmq_entry->read;

	if (count == 0 && TAILQ_PREV(pmq_entry, message_queue_head, entries) != NULL) {
		/*
		 * We don't have any more bytes in this message and there is already
		 * a new message pending.
		 */
		return -ENODATA;
	}

	if (*precv_len < count)
		count = *precv_len;

	memcpy(precv_data, &pmq_entry->message[pmq_entry->read], count);
	pmq_entry->read += count;
	*precv_len = count;

	return 0;
}

/*****************************************************************
 *Function name	: mdb_slave_verify_message_and_reply
*/
/**
 * @brief	Verifies a message by evaluating the CHK byte. If the
 *			message is valid, the given reply is sent.
 *
 * @param	handle		Handle identifying caller (returned by mdb_slave_open)
 * @param	precv_data	Pointer to the received message data to be verified
 * @param	precv_len	Pointer to the length of the message data. After returning,
 *						pdata_len contains the actual message length of the data,
 *						i.e., if valid, minus the CHK byte (0..36).
 * @param	psend_data	Reply to be sent if CHK is valid. If this pointer has the
 * 						value MDB_ACK (=NULL), an ACK is sent. Else, the CHK is
 *						calculated and appended to the data pointed to.
 * @param	send_len	Reply length
 *
 * @return	error code
 * @retval	0			Success (message is valid and ACK has been sent)
 * @retval	-EINVAL		Invalid handle or pdata is NULL or *pdata_len is not within 2..37
 * @retval	-ECOMM		CHK byte did not match the sum of the preceding bytes
 * @retval	-EIO		TTY error
*/
int mdb_slave_verify_message_and_reply(int handle, unsigned char *precv_data,
					int *precv_len, unsigned char *psend_data, int send_len)
{
	struct mdb_slave_handle *pmdb = (struct mdb_slave_handle*) handle;
	unsigned char *ptr = precv_data;
	unsigned char chk = 0;
	int ret = 0;

	if (!pmdb || !precv_data || !precv_len || *precv_len < 2 ||
			*precv_len > MAX_MDB_MESSAGE_LENGTH+1)
		return -EINVAL;

	while (ptr-precv_data < *precv_len-1) {
		chk += *ptr;
		ptr++;
	}

	if (chk == *ptr) {
		*precv_len -= 1;
		send_message(pmdb, psend_data, send_len);
	} else
		ret = -ECOMM;

	return ret;
}

/*****************************************************************
 *Function name	: mdb_slave_send_reply
*/
/**
 * @brief	Makes this slave reply to the next received POLL command
 *			with the given data instead of an ACK. The CHK byte is
 *			calculated and appended automatically. This function
 *			returns immediately. If there is already a reply pending,
 *			an error is returned.
 *
 * @param	handle		Handle identifying caller (returned by mdb_slave_open)
 * @param	psend_data	Pointer to the data to be sent to the master
 * @param	send_len	Length of the data to be sent (1..36)
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle or psend_data is NULL or send_len is not within 1..36
 * @retval	-EIO		TTY error
 * @retval	-ENOSPC		There is already a reply pending
 * @retval	-EFAULT		Pollbyte is 0, this function is disabled
*/
int mdb_slave_send_reply(int handle, unsigned char *psend_data, int send_len)
{
	struct mdb_slave_handle *pmdb = (struct mdb_slave_handle*) handle;

	if (!pmdb || !psend_data || send_len < 1 || send_len > MAX_MDB_MESSAGE_LENGTH)
		return -EINVAL;

	if (0 == pmdb->pollbyte)
		return -EFAULT;

	if (pmdb->pending_reply != NULL)
		return -ENOSPC;

	pmdb->pending_reply = malloc(send_len);

	if (pmdb->pending_reply == NULL)
		return -EIO;

	memcpy(pmdb->pending_reply, psend_data, send_len);
	pmdb->pending_reply_length = send_len;

	return 0;
}

/*****************************************************************
 *Function name	: mdb_slave_get_poll_count
*/
/**
 * @brief	Gets the number of poll commands received and acknowledged
 * 			by this slave.
 *
 * @param	handle		Handle identifying caller (returned by mdb_slave_open)
 * @param	pcount		Pointer to integer to store poll count into
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle or invalid pcount pointer
*/
int mdb_slave_get_poll_count(int handle, unsigned int *pcount)
{
	struct mdb_slave_handle *pmdb = (struct mdb_slave_handle*) handle;

	if (!pmdb || !pcount)
		return -EINVAL;

	*pcount = pmdb->poll_count;
	return 0;
}

/*****************************************************************
 *Function name	: mdb_slave_wakeup
*/
/**
 * @brief	Sets the wakeup line on the MDB interface
 *
 * @param	handle		Handle identifying caller (returned by mdb_slave_open)
 * @param	wakeup		true -> sets the line to GND
 *                      false -> sets the line to VCC
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle
 * @retval	-EIO		Could not set line to specified level
*/
int mdb_slave_wakeup(int handle, bool wakeup)
{
	struct mdb_slave_handle *pmdb = (struct mdb_slave_handle*) handle;

	if (!pmdb)
		return -EINVAL;

	if(pmdb->mdb_wakeup_out)
		return assert_wakeup_line(pmdb->mdb_wakeup_out, wakeup);
	else
		return assert_wakeup_line_deprecated(pmdb->tty_dev, wakeup);
}
