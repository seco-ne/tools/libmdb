/*
 * mdb - Library for using the Multi-Drop Bus (MDB) over a MARK/SPACE
 *       parity supporting TTY serial interface.
 *
 * Author: Tim Jaacks  <tim.jaacks@garz-fricke.com>
 * Copyright (C) 2012-2014 Garz & Fricke GmbH
 */

#ifndef MDB_H
#define MDB_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#define DEFAULT_RESET_TIME_MS		100
#define DEFAULT_RECV_TIMEOUT_MS		5
#define MAX_MDB_MESSAGE_LENGTH		36
#define DRIVER_VERSION_SIZE			64

#define ENACK						255

#define MDB_ACK						0x00
#define MDB_NACK					0xFF

struct mdb_master_settings
{
	unsigned int size;				/* size of this struct */
	unsigned int reset_time_ms;		/* MDB reset time */
	unsigned int recv_timeout_ms;	/* timeout between received bytes */
};


/*****************************************************************
 *Function name	: mdb_master_open
*/
/**
 * @brief	Opens the given serial port and configures it for MDB master mode.
 *			This function returns a handle in the first parameter,
 *			which is needed for all further library calls.
 *
 * @param	handle		Pointer to variable to store handle into
 * @param	tty_dev		String containing the TTY device to use (e.g. "ttymxc1")
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid parameter
 * @retval	-EIO		TTY port could not be opened or configured
*/
int mdb_master_open(int *handle, char* tty_dev);

/*****************************************************************
 *Function name	: mdb_master_close
*/
/**
 * @brief	Closes the serial port and frees all resources.
 *
 * @param	handle		Handle identifying caller (returned by mdb_master_open)
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle
*/
int mdb_master_close(int handle);

/*****************************************************************
 *Function name	: mdb_master_reset
*/
/**
 * @brief	Resets the MDB bus by holding the TX-line active for
 *			the configured reset time (default 100ms).
 *
 * @param	handle		Handle identifying caller (returned by mdb_master_open)
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle
*/
int mdb_master_reset(int handle);

/*****************************************************************
 *Function name	: mdb_master_transaction
*/
/**
 * @brief	Sends an MDB message to the given address, including CHK
 *			calculation. Waits for a reply within the configured timeout
 *			(default 5ms) and returns it to the caller. Automatically
 *			verifies the reply and sends ACK or NACK to the client,
 *			depending on whether CHK was OK or not OK.
 *
 * @param	handle		Handle identifying caller (returned by mdb_master_open)
 * @param	adr			Address of the MDB client to communicate with
 * @param	psend_data	Pointer to the data to be sent to the client
 * @param	send_len	Length of the data to be sent (1..36)
 * @param	precv_data	Pointer to the memory where the received data shall be stored
 * @param	precv_len	Pointer to the length of the receive array. After returning,
 *						precv_len contains the length of the received data (1..36).
 *
 * @return	error code
 * @retval	0			Success (*precv_len = 0 -> ACK has been received)
 * @retval	-ENACK		NACK has been received
 * @retval	-EINVAL		Invalid handle or parameter pointers are NULL or send_len
 *						or *precv_len is not within 1..36
 * @retval	-ENOSPC		Receive buffer was to small for received message
 * @retval	-EIO		TTY error
 * @retval	-ETIMEDOUT	Waiting for reply timed out
 * @retval	-ECOMM		Received CHK did not match the sum of the received bytes
 * @retval	-EPROTO		Reply did not have the last byte marked with 9th bit
*/
int mdb_master_transaction(int handle, unsigned char adr, unsigned char *psend_data,
					int send_len, unsigned char *precv_data, int *precv_len);

/*****************************************************************
 *Function name	: mdb_master_wakeup
*/
/**
 * @brief	Sets the wakeup line on the MDB interface
 *
 * @param	handle		Handle identifying caller (returned by mdb_master_open)
 * @param	wakeup		true -> sets the line to GND
 *                      false -> sets the line to VCC
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle
 * @retval	-EIO		Could not set line to specified level
*/
int mdb_master_wakeup(int handle, bool wakeup);

/*****************************************************************
 *Function name	: mdb_master_get_settings
*/
/**
 * @brief	Gets the current configuration of the MDB device
 *
 * @param	handle		Handle identifying caller (returned by mdb_master_open)
 * @param	psettings	Pointer to mdb_master_settings struct with initialized size parameter
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle or invalid psettings pointer
 * @retval	-EMSGSIZE	The size parameter does not match with the library
*/
int mdb_master_get_settings(int handle, struct mdb_master_settings *psettings);

/*****************************************************************
 *Function name	: mdb_master_set_settings
*/
/**
 * @brief	Sets the configuration of the MDB device
 *
 * @param	handle		Handle identifying caller (returned by mdb_master_open)
 * @param	psettings	Pointer to mdb_master_settings struct with initialized size parameter
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle or invalid psettings pointer
 * @retval	-EMSGSIZE	The size parameter does not match with the library
*/
int mdb_master_set_settings(int handle, struct mdb_master_settings *psettings);

/*****************************************************************
 *Function name	: mdb_slave_open
*/
/**
 * @brief	Opens the given serial port and configures it for MDB
 *			slave mode. All POLL messages received on this port are
 *			automatically acknowledged with an ACK as soon as this
 *			function returns until mdb_slave_close is called.
 *			This function returns a handle in the first parameter,
 *			which is needed for all further library calls.
 *			The process priority is increased by calling nice(-15)
 *			implicitly. This is necessary for appropriate bus timings.
 *			If you encounter delays between single bytes of a message
 *			please check priorities of other processes and adjust
 *			your program's priority accordingly. The need for
 *			increasing the priority requires the calling program to
 *			run with root privileges.
 *
 * @param	handle		Pointer to variable to store handle into
 * @param	tty_dev		String containing the TTY device to use (e.g. "ttymxc1")
 * @param	pollbyte	Poll byte to respond to, includes device address in the upper five bits
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid parameter
 * @retval	-EIO		TTY port could not be opened or configured
 * @retval	-EPERM		Process priority could not be set
*/
int mdb_slave_open(int *handle, char* tty_dev, char pollbyte);

/*****************************************************************
 *Function name	: mdb_slave_close
*/
/**
 * @brief	Closes the serial port and frees all resources.
 *
 * @param	handle		Handle identifying caller (returned by mdb_master_open)
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle
*/
int mdb_slave_close(int handle);

/*****************************************************************
 *Function name	: mdb_slave_wait_for_message
*/
/**
 * @brief	Waits for a message from the master addressed to this
 *			slave. If the timeout values are 0, this function returns
 *			immediately. If timeout is NULL, it waits indefinitely.
 *
 * @param	handle		Handle identifying caller (returned by mdb_slave_open)
 * @param	timeout		Timeout
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle
 * @retval	-ETIMEDOUT	No message has been received within the given timeout
*/
int mdb_slave_wait_for_message(int handle, struct timespec *timeout);

/*****************************************************************
 *Function name	: mdb_slave_read_message
*/
/**
 * @brief	Reads bytes of the current message (if there are any)
 *			and returns immediately. Has to be called after
 *			mdb_slave_wait_for_message(). As soon as the start of a
 *			new message is recognized and all bytes of the current
 *			message have been read, this function returns ENODATA
 *			until mdb_slave_wait_for_message() is called again.
 *			Since this function cannot recognize the end of the
 *			message by itself, it does not evaluate and verify the
 *			CHK byte. Thus, maximum data length is 37.
 *
 * @param	handle		Handle identifying caller (returned by mdb_slave_open)
 * @param	precv_data	Pointer to the memory where the received data shall be stored
 * @param	precv_len	Pointer to the length of the receive array. After returning,
 *						precv_len contains the length of the received data (0..37).
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle or precv_data is NULL or *precv_len is not within 1..37
 * @retval	-ENXIO		No message available, call mdb_slave_wait_for_message() before
 * @retval	-ENODATA	No more bytes left in this message
 * @retval	-EIO		TTY error
*/
int mdb_slave_read_message(int handle, unsigned char *precv_data, int *precv_len);

/*****************************************************************
 *Function name	: mdb_slave_verify_message_and_reply
*/
/**
 * @brief	Verifies a message by evaluating the CHK byte. If the
 *			message is valid, the given reply is sent.
 *
 * @param	handle		Handle identifying caller (returned by mdb_slave_open)
 * @param	precv_data	Pointer to the received message data to be verified
 * @param	precv_len	Pointer to the length of the message data. After returning,
 *						pdata_len contains the actual message length of the data,
 *						i.e., if valid, minus the CHK byte (0..36).
 * @param	psend_data	Reply to be sent if CHK is valid. If this pointer has the
 * 						value MDB_ACK (=NULL), an ACK is sent. Else, the CHK is
 *						calculated and appended to the data pointed to.
 * @param	send_len	Reply length
 *
 * @return	error code
 * @retval	0			Success (message is valid and ACK has been sent)
 * @retval	-EINVAL		Invalid handle or pdata is NULL or *pdata_len is not within 2..37
 * @retval	-ECOMM		CHK byte did not match the sum of the preceding bytes
 * @retval	-EIO		TTY error
*/
int mdb_slave_verify_message_and_reply(int handle, unsigned char *precv_data,
					int *precv_len, unsigned char *psend_data, int send_len);

/*****************************************************************
 *Function name	: mdb_slave_send_reply
*/
/**
 * @brief	Makes this slave reply to the next received POLL command
 *			with the given data instead of an ACK. The CHK byte is
 *			calculated and appended automatically. This function
 *			returns immediately. If there is already a reply pending,
 *			an error is returned.
 *
 * @param	handle		Handle identifying caller (returned by mdb_slave_open)
 * @param	psend_data	Pointer to the data to be sent to the master
 * @param	send_len	Length of the data to be sent (1..36)
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle or psend_data is NULL or send_len is not within 1..36
 * @retval	-EIO		TTY error
 * @retval	-ENOSPC		There is already a reply pending
*/
int mdb_slave_send_reply(int handle, unsigned char *psend_data, int send_len);

/*****************************************************************
 *Function name	: mdb_slave_get_poll_count
*/
/**
 * @brief	Gets the number of poll commands received and acknowledged
 * 			by this slave.
 *
 * @param	handle		Handle identifying caller (returned by mdb_slave_open)
 * @param	pcount		Pointer to integer to store poll count into
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle or invalid pcount pointer
*/
int mdb_slave_get_poll_count(int handle, unsigned int *pcount);

/*****************************************************************
 *Function name	: mdb_slave_wakeup
*/
/**
 * @brief	Sets the wakeup line on the MDB interface
 *
 * @param	handle		Handle identifying caller (returned by mdb_slave_open)
 * @param	wakeup		true -> sets the line to GND
 *                      false -> sets the line to VCC
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid handle
 * @retval	-EIO		Could not set line to specified level
*/
int mdb_slave_wakeup(int handle, bool wakeup);

/*****************************************************************
 *Function name	: mdb_get_version
*/
/**
 * @brief	Writes driver version into version string.
 *
 * @param	pversion	Pointer to version string variable.
 * @param	len			Length of the version string.
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid version pointer
*/
int mdb_get_version(char* pversion, int len);


/*
 * These types and functions are deprecated. They are defined here for
 * backward compatibility only.
 */
#define mdb_settings mdb_master_settings
int mdb_open(int *handle, char* tty_dev);
int mdb_close(int handle);
int mdb_reset(int handle);
int mdb_transaction(int handle, unsigned char adr, unsigned char *psend_data,
					int send_len, unsigned char *precv_data, int *precv_len);
int mdb_wakeup(int handle, bool wakeup);
int mdb_get_settings(int handle, struct mdb_settings *psettings);
int mdb_set_settings(int handle, struct mdb_settings *psettings);

#ifdef __cplusplus
}
#endif

#endif // MDB_H
