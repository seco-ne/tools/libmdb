/*
 * mdb - Library for using the Multi-Drop Bus (MDB) over a MARK/SPACE
 *       parity supporting TTY serial interface.
 *       Common functions and definitions
 *
 * Author: Tim Jaacks  <tim.jaacks@garz-fricke.com>
 * Copyright (C) 2012-2014 Garz & Fricke GmbH
 */
#define _GNU_SOURCE

#include <asm/ioctls.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <gpiod.h>
#include <linux/serial.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#include "libmdb.h"
#include "libmdb_common.h"

#define LIB_NAME		"MDB Library"
#define MAJOR_VERSION	"2"
#define MINOR_VERSION	"0"

/*****************************************************************
 *Function name	: mdb_get_version
*/
/**
 * @brief	Writes driver version into a char array.
 *
 * @param	pversion	Pointer to version string variable.
 * @param	len			Length of the version string.
 *
 * @return	error code
 * @retval	0			Success
 * @retval	-EINVAL		Invalid version pointer
*/
int mdb_get_version(char* pversion, int len)
{
	const char version[] = {LIB_NAME " version " MAJOR_VERSION "." MINOR_VERSION " [r0]"};
	
	if(!pversion || len > DRIVER_VERSION_SIZE || len < sizeof(version))
	return -EINVAL;
	
	memcpy(pversion, &version, sizeof(version));
	return 0;	
}

unsigned char calculate_chk(unsigned char *buffer, int size)
{
	unsigned char chk = 0, *buf_ptr;
	buf_ptr = buffer;
	while (buf_ptr < buffer + size)
		chk += *(buf_ptr++);
	return chk;
}

/* Using the old sys fs entries to access to mdb_wakeup_out gpio */
int assert_wakeup_line_deprecated( const char *port, bool state)
{
	int wakeup_fd, bytes_written;
	char wakeup_dev_node[70];
	char *data = state ? "1\n" : "0\n";

	/*
	 * The wakeup GPIO pin should be exported to sysfs by the board's init
	 * routine. It should have placed a link to the pin under the device's
	 * sysfs folder, called "mdb_wakup_out". We try to open it here. If the
	 * link does not exist, we cannot access the wakeup line.
	 */
	sprintf(wakeup_dev_node, "/sys/class/tty/%s/device/mdb_wakeup_out/value", port);
	wakeup_fd = open(wakeup_dev_node, O_RDWR | O_NONBLOCK);
	if (wakeup_fd < 0)
		return -EIO;

	bytes_written = write(wakeup_fd, data, strlen(data)+1);
	if (bytes_written != strlen(data)+1)
		return -EIO;

	return 0;
}

struct gpiod_line * mdb_open_wakeup_out()
{
	const char * gpio_line_name = "mdb-wakeup-out";
	struct gpiod_chip *chip;
	struct gpiod_line * line = NULL;
	char *path;
	int ret;
	int chip_no = 0;

	while( true)
	{
		ret = asprintf(&path, "/dev/gpiochip%u", chip_no);
		if (!ret)
			return NULL;

		chip = gpiod_chip_open(path);
		free(path);

		if( !chip)
			return NULL;

		line = gpiod_chip_find_line(chip, gpio_line_name);
		if ( line ) 
			break;
		chip_no ++;
	}

	if( line == 0){
		return NULL;
	}

	if( 0 != gpiod_line_request_output(line, "libmdb", 0))
	{
		mdb_close_wakeup_out(line);
		return NULL;
	}

	return line;
}

void mdb_close_wakeup_out(struct gpiod_line * line)
{
	if( !line) return;
	struct gpiod_chip *chip;
	gpiod_line_close_chip(line);
}

int assert_wakeup_line( struct gpiod_line * line, bool state)
{
	int ret = gpiod_line_set_value( line, state);
	if (ret != 0) return -EIO;
	return 0;
}


int init_serial_port(char *tty_dev, int *tty_fd)
{
	int ret = 0;
	char tty_dev_node[30];
	struct termios tio;
	struct serial_struct serial;

	/* Open serial port */
	sprintf(tty_dev_node, "/dev/%s", tty_dev);
	*tty_fd = open(tty_dev_node, O_RDWR | O_NONBLOCK);
	if (*tty_fd < 0)
		return -EIO;

	/* Set serial port configuration */
	tcgetattr(*tty_fd, &tio);
	tio.c_cflag = CS8 | CREAD | PARENB | CMSPAR; // 8 data bits, 1 stop bit
	tio.c_iflag = INPCK | PARMRK;
	tio.c_lflag = 0;
	tio.c_oflag = 0;
	cfsetospeed(&tio, B9600);
	cfsetispeed(&tio, B9600);
	tio.c_cc[VMIN] = 0;		// Non-blocking read:
	tio.c_cc[VTIME] = 0;	// read() will return immediately
	ret = tcsetattr(*tty_fd, TCSANOW, &tio);

	if(ret) {
		ret = -EIO;
		goto ioctl_failed;
	}

	/* set low latency for serial driver */
	ret = ioctl(*tty_fd, TIOCGSERIAL, &serial);
	if(ret) {
		ret = -EIO;
		goto ioctl_failed;
	}
	serial.flags |= ASYNC_LOW_LATENCY;
 	ret = ioctl(*tty_fd, TIOCSSERIAL, &serial);
	if(ret) {
		ret = -EIO;
		goto ioctl_failed;
	}

	return 0;

ioctl_failed:
	close(*tty_fd);
	return ret;
}

/*
 * These types and functions are deprecated. They are defined here for
 * backward compatibility only.
 */
inline int mdb_open(int *handle, char* tty_dev) {
	return mdb_master_open(handle, tty_dev);
}
inline int mdb_close(int handle) {
	return mdb_master_close(handle);
}
inline int mdb_reset(int handle) {
	return mdb_master_reset(handle);
}
inline int mdb_transaction(int handle, unsigned char adr, unsigned char *psend_data,
					int send_len, unsigned char *precv_data, int *precv_len) {
	return mdb_master_transaction(handle, adr, psend_data, send_len, precv_data, precv_len);
}
inline int mdb_wakeup(int handle, bool wakeup) {
	return mdb_master_wakeup(handle, wakeup);
}
inline int mdb_get_settings(int handle, struct mdb_settings *psettings) {
	return mdb_master_get_settings(handle, psettings);
}
inline int mdb_set_settings(int handle, struct mdb_settings *psettings) {
	return mdb_master_set_settings(handle, psettings);
}
