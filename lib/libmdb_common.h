/*
 * mdb - Library for using the Multi-Drop Bus (MDB) over a MARK/SPACE
 *       parity supporting TTY serial interface.
 *
 * Author: Tim Jaacks  <tim.jaacks@garz-fricke.com>
 * Copyright (C) 2012-2014 Garz & Fricke GmbH
 */

#ifndef MDB_COMMON_H
#define MDB_COMMON_H

#include <gpiod.h>
#define MDB_ADDRESS_MASK			0xF8

int assert_wakeup_line_deprecated( const char *port, bool state);
int assert_wakeup_line( struct gpiod_line * line, bool state);
struct gpiod_line * mdb_open_wakeup_out();
void mdb_close_wakeup_out(struct gpiod_line * line);
int init_serial_port(char *tty_dev, int *tty_fd);
unsigned char calculate_chk(unsigned char *buffer, int size);

#endif // MDB_COMMON_H
